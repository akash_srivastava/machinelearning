#changing the directory
import os
os.getcwd()
os.chdir('/Users/akashsrivastava/Desktop/Machine Learning/Udemy ML/Machine Learning A-Z Template Folder/Part 1 - Data Preprocessing/Section 2 -------------------- Part 1 - Data Preprocessing --------------------/Data_Preprocessing')

#importing the libraries
import numpy as np
import pandas as pd

# reading the dataset
dataset = pd.read_csv("Data.csv")
X = dataset.iloc[:,:-1].values
y = dataset.iloc[:,3].values

# Handling missing data
from sklearn.preprocessing import Imputer
imputer  = Imputer(missing_values='NaN',strategy='mean',axis= 0)
imputer.fit(X[:,1:3])
X[:,1:3] = imputer.transform(X[:,1:3])


#Encoding the categorical_data
from sklearn.preprocessing import OneHotEncoder,LabelEncoder
labelEncoder_X = LabelEncoder()
X[:,0] = labelEncoder_X.fit_transform(X[:,0])
labelEncoder_Y = LabelEncoder()
y = labelEncoder_Y.fit_transform(y)

onehotEncoder = OneHotEncoder(categorical_features=[0])
X = onehotEncoder.fit_transform(X).toarray()


#train test split
from sklearn.cross_validation import train_test_split
X_train, X_test,y_train,y_test = train_test_split(X,y,test_size = .3,random_state= 0 )

#feature scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)



























