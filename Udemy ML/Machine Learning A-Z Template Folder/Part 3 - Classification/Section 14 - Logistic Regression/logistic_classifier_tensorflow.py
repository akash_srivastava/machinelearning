#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 25 02:52:23 2017

@author: akashsrivastava
"""

import os
os.getcwd()
os.chdir('/Users/akashsrivastava/Desktop/Machine Learning/Udemy ML/Machine Learning A-Z Template Folder/Part 3 - Classification/Section 14 - Logistic Regression')



import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
#import matplot.pylot as plt

dataset = pd.read_csv('Social_Network_Ads.csv')

X = dataset.iloc[:,1:-1].values
y =  dataset.iloc[:,4:5].values

from sklearn.preprocessing import LabelEncoder,OneHotEncoder
encoder = LabelEncoder()
oneHotEncoder = OneHotEncoder(categorical_features = [0])
X[:,0] = encoder.fit_transform(X[:,0])
X = oneHotEncoder.fit_transform(X).toarray()


from sklearn.cross_validation import train_test_split
X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=.2,random_state = 0)

n = X_train.shape[0]
features = 4
learning_rate = 0.01
training_epochs = 200
batch_size = 100
display_step = 50
n_hidden_1  = 2

X_ = tf.placeholder(tf.float32 ,shape = (None,features))
Y_ = tf.placeholder(tf.float32,[None])

W = tf.random_normal([ features,n_hidden_1], stddev=0.35,name="weights1")
b = tf.Variable(tf.zeros([n_hidden_1], dtype=np.float32), name="bias")

pred = tf.nn.softmax(tf.matmul(X_,W) + b)
cost = tf.reduce_mean(-tf.reduce_sum(Y_*tf.log(pred) ,reduction_indices=1))

optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    
    for epoch in range(training_epochs):
        for (x,y) in zip(X_train,y_train):
            x = np.reshape(x,(1,features))
            #y = np.reshape(y,(None,1))
            _,cost = sess.run([optimizer,cost],feed_dict = {X_ :x,Y_:y})
            
    
        if (epoch+1) % display_step == 0:
            print("Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(cost))
            
    print("Optimization finised")
    
     # Test model
    correct_prediction = tf.equal(tf.argmax(Y_, 1), tf.argmax(pred, 1))
    # Calculate accuracy
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    print("Accuracy:", accuracy.eval({X_: X_test, Y_: y_test}))










