#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 25 02:23:39 2017

@author: akashsrivastava
"""
import os
os.getcwd()
os.chdir('/Users/akashsrivastava/Desktop/Machine Learning/Udemy ML/Machine Learning A-Z Template Folder/Part 3 - Classification/Section 14 - Logistic Regression')



import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
#import matplot.pylot as plt

dataset = pd.read_csv('Social_Network_Ads.csv')

X = dataset.iloc[:,1:-1].values
y =  dataset.iloc[:,-1].values

from sklearn.preprocessing import LabelEncoder,OneHotEncoder
encoder = LabelEncoder()
oneHotEncoder = OneHotEncoder(categorical_features = [0])
X[:,0] = encoder.fit_transform(X[:,0])
X = oneHotEncoder.fit_transform(X).toarray()


from sklearn.cross_validation import train_test_split
X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=.2,random_state = 0)

from sklearn.linear_model import LogisticRegression
classifier  = LogisticRegression()
classifier.fit(X_train,y_train)

predicted_data = classifier.predict(X_test)

decision_function = classifier.decision_function(X_train)
score = classifier.score(X_test,y_test)