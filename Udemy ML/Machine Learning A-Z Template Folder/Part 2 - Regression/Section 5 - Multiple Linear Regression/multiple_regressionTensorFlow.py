#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 23 00:57:13 2017

@author: akashsrivastava
"""

#changing the directory
import os
os.getcwd()
os.chdir('/Users/akashsrivastava/Desktop/Machine Learning/Udemy ML/Machine Learning A-Z Template Folder/Part 2 - Regression/Section 5 - Multiple Linear Regression')

#importing the libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# reading the dataset
dataset = pd.read_csv("50_Startups.csv")
X = dataset.iloc[:,:-1].values
y = dataset.iloc[:,-1].values


learning_rate =  0.05
training_epochs = 1000
display_step = 50

from sklearn.preprocessing import OneHotEncoder,LabelEncoder
encoder = OneHotEncoder(categorical_features = [3])
labelEncoder = LabelEncoder()
X[:,3] = labelEncoder.fit_transform(X[:,3])
X = encoder.fit_transform(X).toarray()

from sklearn.cross_validation import train_test_split
X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=.2,random_state = 0)


#feature scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)


import tensorflow as tf

m = len(X_train)
n = 6



n_hidden_1 = 2
n_hidden_2 = 4
n_hidden_3 = 8


#W1 = tf.Variable(np.random.randn(),name="weights1")
W = tf.random_normal([ n,n_hidden_1], stddev=0.35,name="weights1")
W2 = tf.random_normal([ n_hidden_1,n_hidden_2], stddev=0.9,name="weights1")
W3 = tf.random_normal([ n_hidden_2,n_hidden_3], stddev=0.1,name="weights1")

#W = tf.Variable(W1.initialized_value(), name="w2")
b = tf.Variable(tf.zeros([n_hidden_1], dtype=np.float32), name="bias")
b2 = tf.Variable(tf.zeros([n_hidden_2], dtype=np.float32), name="bias")
b3 = tf.Variable(tf.zeros([n_hidden_3], dtype=np.float32), name="bias")

X_ = tf.placeholder(tf.float32, shape=(None, 6))
Y_ = tf.placeholder(tf.float32)


layer1 = tf.nn.relu(tf.matmul(X_,W)  + b)
layer2 = tf.nn.relu(tf.matmul(layer1,W2) + b2)
layer3 = tf.nn.relu(tf.matmul(layer2,W3) + b3)
pred = tf.transpose(layer3)

loss = tf.reduce_sum(tf.square(Y_ - pred))
optimizer = tf.train.AdamOptimizer(learning_rate).minimize(loss)
init = tf.global_variables_initializer()


with tf.Session() as sess:
    sess.run(init)
    
    for epoch in range(training_epochs):
        for (x,y) in zip(X_train,y_train):
            this_x = np.reshape(x,(1, n))
            sess.run(optimizer,feed_dict = {X_:this_x,Y_:y})
            
        
        if (epoch+1) % display_step == 0:
            c = sess.run(loss, feed_dict={X_: X_train, Y_:y_train})
            print("Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(c), \
                "W=", sess.run(W), "b=", sess.run(b))

    print("Optimization Finished!")
    
    mean2 = sess.run(tf.sqrt(tf.reduce_sum(tf.square(Y_-pred))),feed_dict = {X_:X_test,Y_:y_test})
    print( mean2 )
    
    sess.close()
    





