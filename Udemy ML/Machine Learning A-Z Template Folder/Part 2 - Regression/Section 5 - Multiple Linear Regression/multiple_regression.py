"""
Created on Wed May 17 02:22:13 2017

@author: akashsrivastava
"""

#changing the directory
import os
os.getcwd()
os.chdir('/Users/akashsrivastava/Desktop/Machine Learning/Udemy ML/Machine Learning A-Z Template Folder/Part 2 - Regression/Section 5 - Multiple Linear Regression')

#importing the libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# reading the dataset
dataset = pd.read_csv("50_Startups.csv")
X = dataset.iloc[:,:-1].values
y = dataset.iloc[:,-1].values


from sklearn.preprocessing import OneHotEncoder,LabelEncoder
encoder = OneHotEncoder(categorical_features = [3])
labelEncoder = LabelEncoder()
X[:,3] = labelEncoder.fit_transform(X[:,3])
X = encoder.fit_transform(X).toarray()


from sklearn.cross_validation import train_test_split
X_train,X_test,y_train,y_test = train_test_split(X,y,test_size=.2,random_state = 0)


from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train,y_train)

y_pred = regressor.predict(X_test)


mean = np.sqrt(np.mean(np.square(y_pred-y_test)))







