#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 25 02:09:11 2017

@author: akashsrivastava
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 23 02:48:25 2017

@author: akashsrivastava
"""

#changing the directory
import os
os.getcwd()
os.chdir('/Users/akashsrivastava/Desktop/Machine Learning/Udemy ML/Machine Learning A-Z Template Folder/Part 2 - Regression/Section 6 - Polynomial Regression')

#importing the libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# reading the dataset
dataset = pd.read_csv("Position_Salaries.csv")
X = dataset.iloc[:,1:2].values
y = dataset.iloc[:,2].values


from sklearn.cross_validation import train_test_split
X_train,X_test,y_train,y_test = train_test_split(X,y,test_size = 0,random_state = 0)


# Linear Regressor
from sklearn.linear_model import LinearRegression
linear_regressor = LinearRegression()
linear_regressor.fit(X_train,y_train)
mean = np.sqrt(np.mean(np.square(y_train,linear_regressor.predict(X_train))))


# polynomial Regressor
from sklearn.preprocessing import PolynomialFeatures
poly_reg = PolynomialFeatures(degree = 2)
X_poly = poly_reg.fit_transform(X_train)
poly_reg.fit(X_poly,y)

regressor = LinearRegression()
regressor.fit(X_poly,y_train)



plt.scatter(X_train,y_train,color="blue")
plt.scatter(X_train,regressor.predict(poly_reg.fit_transform(X_train)),color="green")
#plt.plot(X_train,poly_reg.predict(),color="red")

plt.show()

