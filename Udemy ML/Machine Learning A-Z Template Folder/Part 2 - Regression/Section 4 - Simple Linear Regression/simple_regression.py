#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 17 02:22:13 2017

@author: akashsrivastava
"""

#changing the directory
import os
os.getcwd()
os.chdir('/Users/akashsrivastava/Desktop/Machine Learning/Udemy ML/Machine Learning A-Z Template Folder/Part 2 - Regression/Section 4 - Simple Linear Regression')

#importing the libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# reading the dataset
dataset = pd.read_csv("Salary_Data.csv")
X = dataset.iloc[:,:-1].values
y = dataset.iloc[:,1].values


# Handling missing data
"""
from sklearn.preprocessing import Imputer
imputer  = Imputer(missing_values='NaN',strategy='mean',axis= 0)
imputer.fit(X[:,1:3])
X[:,1:3] = imputer.transform(X[:,1:3])
"""

#Encoding the categorical_data
"""
from sklearn.preprocessing import OneHotEncoder,LabelEncoder
labelEncoder_X = LabelEncoder()
X[:,0] = labelEncoder_X.fit_transform(X[:,0])
labelEncoder_Y = LabelEncoder()
y = labelEncoder_Y.fit_transform(y)

onehotEncoder = OneHotEncoder(categorical_features=[0])
X = onehotEncoder.fit_transform(X).toarray()
"""

#train test split
from sklearn.cross_validation import train_test_split
X_train, X_test,y_train,y_test = train_test_split(X,y,test_size = .2,random_state= 0 )


"""
#feature scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
"""

#Simple linear regressor
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train,y_train)

y_pred = regressor.predict(X_test)


#Graphing
'''
plt.scatter(X_train,y_train,color="red")
plt.plot(X_train,regressor.predict(X_train),color="blue")
plt.title("Salary vs Experience")
plt.xlabel("Experience")
plt.ylabel("Salary")
plt.show()
'''

regressor.score(X_test,y_pred)



























