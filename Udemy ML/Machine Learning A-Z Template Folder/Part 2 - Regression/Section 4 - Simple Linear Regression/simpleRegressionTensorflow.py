#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 22 19:58:40 2017

@author: akashsrivastava
"""
import os
os.getcwd()
os.chdir('/Users/akashsrivastava/Desktop/Machine Learning/Udemy ML/Machine Learning A-Z Template Folder/Part 2 - Regression/Section 4 - Simple Linear Regression')


#importing the libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf

learning_rate = 0.0001
training_epochs = 1000
display_step = 50


dataset =  pd.read_csv("Salary_Data.csv")
X = dataset.iloc[:,:-1].values
y = dataset.iloc[:,1].values



#train test split
from sklearn.cross_validation import train_test_split
train_X, test_X,train_Y,test_Y = train_test_split(X,y,test_size = .2,random_state= 0 )


#Weight and bias for the model
W = tf.Variable(np.random.randn(),name="weights")
b = tf.Variable(np.random.randn(),name="bias")

# tf Graph Input
X = tf.placeholder("float")
Y = tf.placeholder("float")

pred = tf.add(tf.multiply(X,W),b)
pred = tf.transpose(pred)
cost = tf.reduce_sum(tf.square(Y-pred))
optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)




# Initializing the variables
init = tf.global_variables_initializer()

# Launch the graph
with tf.Session() as sess:
    sess.run(init)

    # Fit all training data
    for epoch in range(training_epochs):
        for (x, y) in zip(train_X, train_Y):
            sess.run(optimizer, feed_dict={X: x, Y: y})

        # Display logs per epoch step
        if (epoch+1) % display_step == 0:
            c = sess.run(cost, feed_dict={X: train_X, Y:train_Y})
            print("Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(c), \
                "W=", sess.run(W), "b=", sess.run(b))

    print("Optimization Finished!")
    training_cost = sess.run(cost, feed_dict={X: train_X, Y: train_Y})
    print("Training cost=", training_cost, "W=", sess.run(W), "b=", sess.run(b), '\n')
    
    

    print("Testing... (Mean square loss Comparison)")
    testing_cost = sess.run(
        tf.reduce_sum(tf.square(Y-pred)),
        feed_dict={X: test_X, Y: test_Y})  # same function as cost above
    print("Testing cost=", testing_cost)
    print("Absolute mean square loss difference:", abs(
        training_cost - testing_cost))
    

'''    # Graphic display
    plt.plot(train_X, train_Y, 'ro', label='Original data')
    plt.plot(train_X, sess.run(W) * train_X + sess.run(b), label='Fitted line')
    plt.legend()
    plt.show()
'''    
    

'''
    plt.plot(test_X, test_Y, 'bo', label='Testing data')
    plt.plot(train_X, sess.run(W) * train_X + sess.run(b), label='Fitted line')
    plt.legend()
    plt.show()
'''
    